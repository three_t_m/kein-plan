package MainClass;

import SortConcurrent.ConcurrentImprovedMergesort;
import SortConcurrent.ConcurrentImprovedQuicksort;
import SortConcurrent.ConcurrentMergesort;
import SortConcurrent.ConcurrentQuicksort;
import SortStandard.StandardImprovedMergesort;
import SortStandard.StandardImprovedQuicksort;
import SortStandard.StandardMergesort;
import SortStandard.StandardQuicksort;
import Utility.ExecutionTimer;
import Utility.GraphHandler;
import Utility.ScoreObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.DoubleStream;

public class MainClass {
    public static void main(String[] args) throws ExecutionException, InterruptedException
    {
        /*
            Kleiner Kommentar:


            Die Varianten wo der "Basecase" < 50 Elemente als Insertion-Sort implementiert ist, sind etwa doppelt so schnell.
            Ich wollte ursprünglich die Quicksort-Varianten mit meinen Mergesort-Varianten vergleichen. Jedoch musste ich leider feststellen,
             dass die (meine zumindest ;-) ) Mergesort-Methoden um faktoren langsamer waren als die Quicksort Varianten. Im Graph hätte man kaum differenzen gesehen. (Logarithmisch schon...)

        */


        Double[] inputArray;
        Double[] sorted;


        Map<Integer, List<ScoreObject>> scores = new HashMap<Integer, List<ScoreObject>>();
        scores.put(0, new ArrayList<ScoreObject>());
        scores.put(1, new ArrayList<ScoreObject>());
        scores.put(2, new ArrayList<ScoreObject>());
        scores.put(3, new ArrayList<ScoreObject>());

        //Main loop.
        for(double i = 5; i < 250000; i *= 1.05)
        {
            System.out.println("We are at : " + i + " Elements! ");
            inputArray = getArrayWithRandomDobles((int)i);

            //Extrem spannend: die 1. Ausführung im loop dauert immer massiv länger. Die Resultate werden viel konsequenter, wenn man zuerst einmal einen Leerlauf macht.
            new ExecutionTimer<StandardQuicksort>(new StandardQuicksort(inputArray,0, inputArray.length - 1));

            scores.get(0).add(
                    new ScoreObject(
                            "StandardQuicksort",
                            (long)i,
                            new ExecutionTimer<StandardQuicksort>(new StandardQuicksort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(1).add(
                    new ScoreObject(
                            "ConcurrentQuicksort",
                            (long)i,
                            new ExecutionTimer<ConcurrentQuicksort>(new ConcurrentQuicksort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(2).add(
                    new ScoreObject(
                            "StandardImprovedQuicksort",
                            (long)i,
                            new ExecutionTimer<StandardImprovedQuicksort>(new StandardImprovedQuicksort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(3).add(
                    new ScoreObject(
                            "ConcurrentImprovedQuicksort",
                            (long)i,
                            new ExecutionTimer<ConcurrentImprovedQuicksort>(new ConcurrentImprovedQuicksort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
        }

        GraphHandler graphHandler = new GraphHandler(scores);
        graphHandler.createAndShowGui();
    }

    private static Double[] getArrayWithRandomDobles(int size)
    {
        Double array[] = new Double[size];

        for (int i = 0; i < size; i++){
            array[i] = Math.random() * 1;
        }
        return array;
    }
}
