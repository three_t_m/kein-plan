package InsertionSort;

public class InsertionSort{

     public static <T extends Comparable> T[] insertionsortImpl (T[] toSort) {
         T tmp;
         if ( toSort.length > 1) {
             for (int j =1; j < toSort.length; j ++) { // iterate right
                 T value = toSort[j]; // read right element
                 int i = j -1; // define initial left position
                 while (i >=0 && toSort[i].compareTo(value) > 0) {
                     tmp = toSort[i + 1];
                     toSort[i+1] = toSort[i];
                     toSort[i] = tmp;
                     i --;
                     }
                 toSort[i+1] = value;
             }
         }
         return toSort;
     }
}
