package SortStandard;

import InsertionSort.InsertionSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class StandardImprovedMergesort<ElementType extends Comparable> implements DivideAndConquerableArray<ElementType>, Supplier<ElementType[]> {

    ElementType[] data;
    ElementType[] aux;
    int left;
    int right;

    int mid;
    int i;
    int j;
    int k;

    public StandardImprovedMergesort(ElementType[] data, int left, int right)
    {
        this.data = data;
        this.aux = data.clone();

        this.left = left;
        this.right = right;
    }


    @Override
    public ElementType[] get() {
        return this.divideAndConquer();
    }

    @Override
    //If there are 6 elements left, we switch to insertion Sort
    public boolean isBasic() {
        return  left>= right || right - left < 44 ;
    }

    @Override
    public ElementType[] baseFun() {
        //Sort last few elements by insertion Sort
        ElementType[] temp = InsertionSort.insertionsortImpl(Arrays.copyOfRange(data, left, right));
        System.arraycopy(temp, 0, data , left , temp.length);

        return data;
    }

    @Override
    public List<? extends DivideAndConquerableArray<ElementType>> decompose() {
        mid = ( left + right ) / 2;
        i = left ; // sorted left - half start
        j = mid +1; // sorted right - half start
        k = left ; // sorted merged - halves start

        List<StandardImprovedMergesort<ElementType>> parts = new ArrayList<>();

        parts.add(new StandardImprovedMergesort(data, left, mid));
        parts.add(new StandardImprovedMergesort(data, mid + 1, right));

        return parts;
    }

    @Override
    public ElementType[] recombine(List<ElementType[]> intermediateResults) {
        data = intermediateResults.get(0);
        aux = intermediateResults.get(1).clone();

        while (i <= mid && j <= right ) { // l-r- merge left & right
             if (data[i].compareTo(data[j]) < 0)
                 aux [k ++] = data [ i ++];
             else
             aux [k ++] = data [ j ++];
        }

        // copy possible sorted data left - over into sub -aux
        System . arraycopy ( data , i , aux , k , mid -i +1);
        // copy processed sub - aux into data for output
        System . arraycopy ( aux , left , data , left , j - left );

        return data;
    }
}
