package Utility;

public class ScoreObject {
    private String name;
    private long yValue;
    private long xValue;

    public ScoreObject(String name, long xValue, long yValue)
    {
        this.name = name;
        this.xValue = xValue;
        this.yValue = yValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getyValue() {
        return yValue;
    }

    public void setyValue(long yValue) {
        this.yValue = yValue;
    }

    public long getxValue() {
        return xValue;
    }

    public void setxValue(long xValue) {
        this.xValue = xValue;
    }
}
