package SortConcurrent;

import InsertionSort.InsertionSort;
import SortStandard.DivideAndConquerableArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class ConcurrentImprovedQuicksort<ElementType extends Comparable> implements ConcurrentDivideAndConquerableArrray<ElementType>, Supplier<ElementType[]> {

    ElementType[] data;
    ElementType tempPivot;

    int left;
    int right;


    int mid;

    public ConcurrentImprovedQuicksort(ElementType[] data, int left, int right)
    {
        this.data = data;

        this.left = left;
        this.right = right;
    }

    @Override
    public ElementType[] get() {

        ExecutorService pool = Executors.newFixedThreadPool(8);

        ElementType[] res = this.divideAndConquerConcurrent(pool);

        pool.shutdownNow();

        return res;
    }


    @Override
    //If there are 6 elements left, we switch to insertion Sort
    public boolean isBasic() {
        return  left>= right || right - left < 50 ;
    }

    @Override
    public ElementType[] baseFun() {
        //Sort last few elements by insertion Sort
        ElementType[] temp = InsertionSort.insertionsortImpl(Arrays.copyOfRange(data, left, right));
        System.arraycopy(temp, 0, data , left , temp.length);
        return data;
    }
    
    @Override
    public List<? extends DivideAndConquerableArray<ElementType>> decompose() {
        int median = getMedianOfThree();
        tempPivot = data[right];
        data[right] = data[median];
        data[median] = tempPivot;

        mid = partition();

        List<ConcurrentImprovedQuicksort<ElementType>> parts = new ArrayList<>();

        parts.add(new ConcurrentImprovedQuicksort(data, left, mid - 1));
        parts.add(new ConcurrentImprovedQuicksort(data, mid + 1, right));

        return parts;
    }

    @Override
    public ElementType[] recombine(List<ElementType[]> intermediateResults) {
        return data;
    }

    private int getMedianOfThree()
    {
        if ( right - left +1 >= 3) {
            int mid = ( left + right )/2;
            ElementType leftObject = data[left];
            ElementType midObject = data[mid];
            ElementType rightObject = data[right];
            if ((leftObject.compareTo(midObject)) <= 0) {
                 if ( midObject.compareTo(rightObject ) <= 0)
                     return mid ;
                 else if (rightObject.compareTo(leftObject ) <= 0)
                     return left ;
                 } else if (midObject.compareTo(rightObject ) > 0) {
                 return mid ;
                }
             }
        return right ;
    }

    private int partition()
    {
        ElementType pivot = data[right];
        ElementType tmp;

        int i = left;
        int j = right;

        while (i < j )
        {
            while (i < j && data[i].compareTo(pivot) < 0)
                 i ++; // move right ( paint green ) in left partition
            while (j > i && data[j].compareTo(pivot) >= 0)
                j--; // move left ( paint orange ) in right partition
            if (i <j){
                tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
            }
        }

        // "orange - yellow swap "
        tmp = data[i];
        data[i] = data[right];
        data[right] = tmp;

        return i; // return mid - element
    }
}
