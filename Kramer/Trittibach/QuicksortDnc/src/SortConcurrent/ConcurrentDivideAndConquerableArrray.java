package SortConcurrent;

import SortStandard.DivideAndConquerableArray;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public interface ConcurrentDivideAndConquerableArrray<OutputType> extends DivideAndConquerableArray<OutputType>, Callable<OutputType[]> {
    default OutputType[] call() {
        return divideAndConquer();
    }

    default OutputType[] divideAndConquerConcurrent(ExecutorService executor) {
        if (this.isBasic()) return this.baseFun();

        List<? extends DivideAndConquerableArray<OutputType>>
                subcomponents = this.decompose();

        List<Future<OutputType[]>> intermediateResults = new ArrayList<Future<OutputType[]>>(subcomponents.size());

        subcomponents.forEach(
                subcomponent -> intermediateResults.add(
                        executor.submit(((ConcurrentDivideAndConquerableArrray) subcomponent))
                ));

        List<OutputType[]> list = new ArrayList<OutputType[]>();
        for (Future<OutputType[]> f : intermediateResults) {
            try {
                list.add(f.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return recombine(list);
    }
}

