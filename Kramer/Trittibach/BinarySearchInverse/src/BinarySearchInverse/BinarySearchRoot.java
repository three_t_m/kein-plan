package BinarySearchInverse;

import java.util.function.Supplier;

public class BinarySearchRoot implements Supplier<Double> {

    private double number;
    private int precision;
    private double target;


    public BinarySearchRoot(double number, int precision)
    {

        this.number = number;
        this.precision = precision;

        this.target = Math.sqrt(number);

    }

    @Override
    public Double get() {
        if(number == 1)
        {
            return 0d;
        }
        return CalculateBinaryRoot(0, number, precision);
    }

    private double CalculateBinaryRoot(double left, double right, int precision)
    {
        double mid = ( left + right )/2;

        if(precision == 0)
        {
            return mid;
        }

        if ( right == left ) {
            return left;
        }
        else if ( mid == target ||  left == mid )
        {
           return mid ;
        }
        else if ( target < mid) {
            return CalculateBinaryRoot( left, mid, --precision);
        }
         else {
            return CalculateBinaryRoot(mid, right,  --precision);

        }
    }
}
