package BinarySearchInverse;

import java.util.function.Supplier;

public class BinarySearchLog implements Supplier<Double> {

    private double number;
    private int precision;
    private double target;


    public BinarySearchLog(double number, int precision)
    {

        this.number = number;
        this.precision = precision;

        this.target = Math.log(number) / Math.log(2);

    }

    @Override
    public Double get() {
        if(number == 1)
        {
            return 1d;
        }
        return CalculateBinaryLog(0, number, precision);
    }

    private double CalculateBinaryLog(double left, double right, int precision)
    {
        double mid = ( left + right )/2;

        if(precision == 0)
        {
            return mid;
        }

        if ( right == left ) {
            return left;
        }
        else if ( mid == target ||  left == mid )
        {
           return mid ;
        }
        else if ( target < mid) {
            return CalculateBinaryLog( left, mid, --precision);
        }
         else {
            return CalculateBinaryLog(mid, right,  --precision);

        }
    }
}
