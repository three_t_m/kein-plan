package MainClass;

import BinarySearchInverse.BinarySearchLog;
import BinarySearchInverse.BinarySearchRoot;
import Utility.ExecutionTimer;

import java.util.concurrent.ExecutionException;

public class MainClass {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Binary logarithms approximation with Binary Search:");
        System.out.println("");

        System.out.println("Result of binary log(5) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 2)).result));
        System.out.println("Result of binary log(5) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 4)).result));
        System.out.println("Result of binary log(5) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 8)).result));

        System.out.println("Result of binary log(5) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 10)).result));
        System.out.println("Result of binary log(5) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 100)).result));
        System.out.println("Result of binary log(5) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 1000)).result));

        System.out.println("");


        System.out.println("Time of binary log(5) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 2)).time));
        System.out.println("Time of binary log(5) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 4)).time));
        System.out.println("Time of binary log(5) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 8)).time));

        System.out.println("Time of binary log(5) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 10)).time));
        System.out.println("Time of binary log(5) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 100)).time));
        System.out.println("Time of binary log(5) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchLog(5d, 1000)).time));

        System.out.println("");

        System.out.println("Result of binary log(2147483647.1) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 2)).result));
        System.out.println("Result of binary log(2147483647.1) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 4)).result));
        System.out.println("Result of binary log(2147483647.1) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 8)).result));

        System.out.println("Result of binary log(2147483647.1) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 10)).result));
        System.out.println("Result of binary log(2147483647.1) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 100)).result));
        System.out.println("Result of binary log(2147483647.1) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 1000)).result));

        System.out.println("");

        System.out.println("Time of binary log(2147483647.1) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 2)).time));
        System.out.println("Time of binary log(2147483647.1) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 4)).time));
        System.out.println("Time of binary log(2147483647.1) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 8)).time));

        System.out.println("Time of binary log(2147483647.1) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 10)).time));
        System.out.println("Time of binary log(2147483647.1) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 100)).time));
        System.out.println("Time of binary log(2147483647.1) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchLog(2147483647.1, 1000)).time));




        System.out.println("Square Root approximation with Binary Search:");
        System.out.println("");

        System.out.println("Result of sqrt(5) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 2)).result));
        System.out.println("Result of sqrt(5) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 4)).result));
        System.out.println("Result of sqrt(5) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 8)).result));

        System.out.println("Result of sqrt(5) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 10)).result));
        System.out.println("Result of sqrt(5) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 100)).result));
        System.out.println("Result of sqrt(5) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 1000)).result));

        System.out.println("");


        System.out.println("Time of sqrt(5) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 2)).time));
        System.out.println("Time of sqrt(5) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 4)).time));
        System.out.println("Time of sqrt(5) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 8)).time));

        System.out.println("Time of sqrt(5) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 10)).time));
        System.out.println("Time of sqrt(5) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 100)).time));
        System.out.println("Time of sqrt(5) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchRoot(5d, 1000)).time));

        System.out.println("");

        System.out.println("Result of sqrt(1000000.1) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 2)).result));
        System.out.println("Result of sqrt(1000000.1) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 4)).result));
        System.out.println("Result of sqrt(1000000.1) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 8)).result));

        System.out.println("Result of sqrt(1000000.1) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 10)).result));
        System.out.println("Result of sqrt(1000000.1) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 100)).result));
        System.out.println("Result of sqrt(1000000.1) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 1000)).result));

        System.out.println("");

        System.out.println("Time of sqrt(1000000.1) Precision: 2: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 2)).time));
        System.out.println("Time of sqrt(1000000.1) Precision: 4: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 4)).time));
        System.out.println("Time of sqrt(1000000.1) Precision: 8: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 8)).time));

        System.out.println("Time of sqrt(1000000.1) Precision: 10: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 10)).time));
        System.out.println("Time of sqrt(1000000.1) Precision: 100: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 100)).time));
        System.out.println("Time of sqrt(1000000.1) Precision: 1000: " + (new ExecutionTimer<Double>(new BinarySearchRoot(1000000.1, 1000)).time));


    }
}
