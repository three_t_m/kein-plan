package ConncurrentFibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class FibonacciDNCConcurrent implements ConcurrentDivideAndConquerable<Integer>, Supplier<Integer> {
    private int n;

    public FibonacciDNCConcurrent(int n) {
        this.n = n;
    }

    @Override
    public boolean isBasic() {

        if (this.n < 3) {
            return true;
        }
        return false;
    }

    @Override
    public Integer get() {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        return this.divideAndConquerConcurrent(executor);
    }

    @Override
    public Integer baseFun() {
        return 1;
    }

    @Override
    public List<FibonacciDNCConcurrent> decompose() {
        List<FibonacciDNCConcurrent> integers = new ArrayList<>();

        integers.add(new FibonacciDNCConcurrent(n - 1));
        integers.add(new FibonacciDNCConcurrent(n - 2));

        return integers;
    }

    @Override
    public Integer recombine(List<Integer> intermediateResults) {
        return intermediateResults.stream().mapToInt(Integer::intValue).sum();
    }
}