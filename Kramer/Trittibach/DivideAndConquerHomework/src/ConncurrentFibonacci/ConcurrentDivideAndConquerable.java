package ConncurrentFibonacci;

import StandardFibonacci.DivideAndConquerable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public interface ConcurrentDivideAndConquerable<OutputType> extends DivideAndConquerable<OutputType>, Callable<OutputType> {
    default OutputType call() {
        return divideAndConquer();
    }

    default OutputType divideAndConquerConcurrent(ExecutorService executorService) {
        if (this.isBasic()) return this.baseFun();

        List<? extends DivideAndConquerable<OutputType>>
                subcomponents = this.decompose();

        List<Future<OutputType>> intermediateResults = new ArrayList<Future<OutputType>>(subcomponents.size());

        subcomponents.forEach(
                subcomponent -> intermediateResults.add(
                        executorService.submit(((ConcurrentDivideAndConquerable) subcomponent))
                ));


        List<OutputType> list = new ArrayList<OutputType>();
        for (Future<OutputType> f : intermediateResults) {
            try {
                list.add(f.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return recombine(list);
    }
}

