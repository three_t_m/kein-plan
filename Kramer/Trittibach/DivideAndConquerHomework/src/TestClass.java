import ConncurrentFibonacci.FibonacciDNCConcurrent;
import DynamicFibonacci.DynamicFibonacci;
import StandardFibonacci.FibonacciDNC;

import java.util.concurrent.ExecutionException;

public class TestClass {
    /**
     * Method does not output the actual values of the fibonacci numbers, as they are not important to us.
     * Method outputs the execution time of each variant of the fibonacci calculation in milliseconds
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("NORMAL");
        FibonacciDNC normalFibonacciDnC;

        ExecutionTimer<Integer> myTimer;

        for (int i = 1; i < 30; i++) {
            normalFibonacciDnC = new FibonacciDNC(i);
            System.out.println("n = " + ((Integer) i).toString() + ": " + new ExecutionTimer<Integer>(normalFibonacciDnC).time / 1000000 + "ms");
        }


        System.out.println("PARALLEL");
        FibonacciDNCConcurrent parallelFibonacciDnc;

        for (int i = 1; i < 30; i++) {
            parallelFibonacciDnc = new FibonacciDNCConcurrent(i);
            System.out.println("n = " + ((Integer) i).toString() + ": " + new ExecutionTimer<Integer>(parallelFibonacciDnc).time / 1000000 + "ms");
        }

        System.out.println("DYNAMIC");
        DynamicFibonacci dynamicFibonacci;

        for (int i = 1; i < 30; i++) {
            dynamicFibonacci = new DynamicFibonacci(i);
            System.out.println("n = " + ((Integer) i).toString() + ": " + new ExecutionTimer<Integer>(dynamicFibonacci).time / 1000000 + "ms");
        }
    }
}