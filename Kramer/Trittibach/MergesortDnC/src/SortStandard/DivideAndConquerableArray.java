package SortStandard;

import java.util.ArrayList;
import java.util.List;

public interface DivideAndConquerableArray<OutputType> {
    boolean isBasic();

    OutputType[] baseFun();

    List<? extends DivideAndConquerableArray<OutputType>> decompose();

    OutputType[] recombine(List<OutputType[]> intermediateResults);

    default List<? extends DivideAndConquerableArray<OutputType[]>>

    stump() {
                return new ArrayList<DivideAndConquerableArray<OutputType[]>>(0);
    }

    default OutputType[] divideAndConquer() {

        if (this.isBasic()) return this.baseFun();

        List<? extends DivideAndConquerableArray<OutputType>>
                subcomponents = this.decompose();

        List<OutputType[]>
                intermediateResults = new ArrayList<OutputType[]>(subcomponents.size());

        subcomponents.forEach(
                subcomponent -> intermediateResults.add(subcomponent.divideAndConquer()));

        return recombine(intermediateResults);
    }
}



