package Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GraphHandler extends JPanel {

    private static final Stroke GRAPH_STROKE = new BasicStroke(2f);

    private int _padding = 25;
    private int _labelPadding = 25;
    private Color _pointColor = new Color(100, 100, 100, 180);
    private Color _gridColor = new Color(200, 200, 200, 200);
    private int _pointWidth = 4;
    private int _numberYDivisions = 10;
    private Map<Integer, List<ScoreObject>> _scores;

    public GraphHandler(Map<Integer, List<ScoreObject>> scores) {
        _scores = scores;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        List<Point> graphPoints = new ArrayList<>();

        for(int n = 0; n < _scores.size(); n++) {

            double xScale = ((double) getWidth() - (2 * _padding) - _labelPadding) / (_scores.get(n).size() - 1);
            double yScale = ((double) getHeight() - 2 * _padding - _labelPadding) / (getMaxScore() - getMinScore());

            for (int i = 0; i < _scores.get(n).size(); i++) {
                int x1 = (int) (i * xScale + _padding + _labelPadding);
                int y1 = (int) ((getMaxScore() - _scores.get(n).get(i).getyValue()) * yScale + _padding);
                graphPoints.add(new Point(x1, y1));
            }

            // draw white background
            g2.setColor(Color.WHITE);
            g2.fillRect(_padding + _labelPadding, _padding, getWidth() - (2 * _padding) - _labelPadding, getHeight() - 2 * _padding - _labelPadding);
            g2.setColor(Color.BLACK);

            // create hatch marks and grid lines for y axis
            for (int i = 0; i < _numberYDivisions + 1; i++) {
                int x0 = _padding + _labelPadding;
                int x1 = _pointWidth + _padding + _labelPadding;
                int y0 = getHeight() - ((i * (getHeight() - _padding * 2 - _labelPadding)) / _numberYDivisions + _padding + _labelPadding);
                int y1 = y0;
                if (_scores.get(n).size() > 0) {
                    g2.setColor(_gridColor);
                    g2.drawLine(_padding + _labelPadding + 1 + _pointWidth, y0, getWidth() - _padding, y1);
                    g2.setColor(Color.BLACK);
                    String yLabel = ((int) ((getMinScore() + (getMaxScore() - getMinScore()) * ((i * 1.0) / _numberYDivisions)) * 100)) / 100.0 + "ms";
                    FontMetrics metrics = g2.getFontMetrics();
                    int labelWidth = metrics.stringWidth(yLabel);
                    g2.drawString(yLabel, x0 - labelWidth - 5, y0 + (metrics.getHeight() / 2) - 3);
                }
                g2.drawLine(x0, y0, x1, y1);
            }

            // and for x axis
            for (int i = 0; i < _scores.get(n).size(); i++) {
                if (_scores.get(n).size() > 1) {
                    int x0 = i * (getWidth() - _padding * 2 - _labelPadding) / (_scores.get(n).size() - 1) + _padding + _labelPadding;
                    int x1 = x0;
                    int y0 = getHeight() - _padding - _labelPadding;
                    int y1 = y0 - _pointWidth;
                    if ((i % ((int) ((_scores.get(n).size() / 20.0)) + 1)) == 0) {
                        g2.setColor(_gridColor);
                        g2.drawLine(x0, getHeight() - _padding - _labelPadding - 1 - _pointWidth, x1, _padding);
                        g2.setColor(Color.BLACK);
                        String xLabel = _scores.get(n).get(i).getxValue() + "";
                        FontMetrics metrics = g2.getFontMetrics();
                        int labelWidth = metrics.stringWidth(xLabel);
                        g2.drawString(xLabel, x0 - labelWidth / 2, y0 + metrics.getHeight() + 3);
                    }
                    g2.drawLine(x0, y0, x1, y1);
                }
            }
        }

        // create x and y axes
        g2.drawLine(_padding + _labelPadding, getHeight() - _padding - _labelPadding, _padding + _labelPadding, _padding);
        g2.drawLine(_padding + _labelPadding, getHeight() - _padding - _labelPadding, getWidth() - _padding, getHeight() - _padding - _labelPadding);

        Stroke oldStroke = g2.getStroke();
        g2.setStroke(GRAPH_STROKE);

        int iterations = 0;

        for (int i = 0; i < graphPoints.size() - 1; i++){

            if (_scores.size() >= 1 && i % _scores.get(1).size() == 0){
                Color col = createRandomColor();
                if(iterations == 0)
                {
                    Ellipse2D.Double circle = new Ellipse2D.Double(40, 10, 10, 10);
                    g2.setColor(col);
                    g2.fill(circle);
                }
                if(iterations == 1)
                {

                    Ellipse2D.Double circle = new Ellipse2D.Double(190, 10, 10, 10);
                    g2.setColor(col);
                    g2.fill(circle);
                }
                if(iterations == 2)
                {

                    Ellipse2D.Double circle = new Ellipse2D.Double(390, 10, 10, 10);
                    g2.setColor(col);
                    g2.fill(circle);

                }
                if(iterations == 3)
                {

                    Ellipse2D.Double circle = new Ellipse2D.Double(590, 10, 10, 10);
                    g2.setColor(col);
                    g2.fill(circle);

                }

                iterations++;
                continue;
            }

            int x1 = graphPoints.get(i).x;
            int y1 = graphPoints.get(i).y;
            int x2 = graphPoints.get(i + 1).x;
            int y2 = graphPoints.get(i + 1).y;

            // don't draw a line between two graphs
            if (x2 > x1){
                g2.drawLine(x1, y1, x2, y2);
            }
        }

        // draw points
        g2.setStroke(oldStroke);
        g2.setColor(_pointColor);
        for (int i = 0; i < graphPoints.size(); i++) {
            int x = graphPoints.get(i).x - _pointWidth / 2;
            int y = graphPoints.get(i).y - _pointWidth / 2;
            int ovalW = _pointWidth;
            int ovalH = _pointWidth;
            g2.fillOval(x, y, ovalW, ovalH);
        }

        g2.drawString("StandardMergesort", 50, 20);

        g2.drawString("ConcurrentMergesort", 200, 20);

        g2.drawString("StandardImprovedMergesort", 400, 20);

        g2.drawString("ConcurrentImprovedMergesort", 600, 20);
    }

    public void setScores(Map<Integer, List<ScoreObject>> scores) {
        _scores = scores;
        invalidate();
        this.repaint();
    }

    public Map<Integer, List<ScoreObject>> getScores() {
        return _scores;
    }

    public void createAndShowGui() {
        setPreferredSize(new Dimension(800, 600));
        JFrame frame = new JFrame("Mergesort");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(this);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private double getMinScore() {
        double minScore = Double.MAX_VALUE;
        for (int i = 0; i < _scores.size(); i++) { // (Double score : scores) {
            for (ScoreObject score : _scores.get(i)){
                minScore = Math.min(minScore, score.getyValue());
            }
        }
        return minScore;
    }

    private double getMaxScore() {
        double maxScore = Double.MIN_VALUE;
        for (int i = 0; i < _scores.size(); i++) { // (Double score : scores) {
            for (ScoreObject score : _scores.get(i)){
                maxScore = Math.max(maxScore, score.getyValue());
            }
        }
        return maxScore;
    }

    private Color createRandomColor() {
        int r = (int)(Math.random()*256);
        int g = (int)(Math.random()*256);
        int b = (int)(Math.random()*256);
        return new Color(r, g, b);
    }
}

