package SortConcurrent;

import SortStandard.DivideAndConquerableArray;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class ConcurrentMergesort<ElementType extends Comparable> implements ConcurrentDivideAndConquerableArrray<ElementType>, Supplier<ElementType[]> {

    ElementType[] data;
    ElementType[] aux;
    int left;
    int right;

    int mid;
    int i;
    int j;
    int k;

    public ConcurrentMergesort(ElementType[] data, int left, int right)
    {
        this.data = data;
        this.aux = data.clone();

        this.left = left;
        this.right = right;
    }


    @Override
    public ElementType[] get() {
        ExecutorService pool = Executors.newFixedThreadPool(8);

        ElementType[] res = this.divideAndConquerConcurrent(pool);

        pool.shutdownNow();

        return res;

    }

    @Override
    public boolean isBasic() {
        return  left >= right ;
    }

    @Override
    public ElementType[] baseFun() {
        return data;
    }

    @Override
    public List<? extends DivideAndConquerableArray<ElementType>> decompose() {
        mid = ( left + right ) / 2;
        i = left ; // sorted left - half start
        j = mid +1; // sorted right - half start
        k = left ; // sorted merged - halves start

        List<ConcurrentMergesort<ElementType>> parts = new ArrayList<>();

        parts.add(new ConcurrentMergesort(data, left, mid));
        parts.add(new ConcurrentMergesort(data, mid + 1, right));

        return parts;
    }

    @Override
    public ElementType[] recombine(List<ElementType[]> intermediateResults) {
        data = intermediateResults.get(0);
        aux = intermediateResults.get(1).clone();

        while (i <= mid && j <= right ) { // l-r- merge left & right
             if (data[i].compareTo(data[j]) < 0)
                 aux [k ++] = data [ i ++];
             else
             aux [k ++] = data [ j ++];
        }

        // copy possible sorted data left - over into sub -aux
        System . arraycopy ( data , i , aux , k , mid -i +1);
        // copy processed sub - aux into data for output
        System . arraycopy ( aux , left , data , left , j - left );

        return data;
    }
}
