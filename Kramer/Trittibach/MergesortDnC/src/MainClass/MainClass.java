package MainClass;

import SortConcurrent.ConcurrentImprovedMergesort;
import SortConcurrent.ConcurrentMergesort;
import SortStandard.StandardImprovedMergesort;
import SortStandard.StandardMergesort;
import Utility.ExecutionTimer;
import Utility.GraphHandler;
import Utility.ScoreObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MainClass {
    public static void main(String[] args) throws ExecutionException, InterruptedException
    {
        Map<Integer, List<ScoreObject>> scores = new HashMap<Integer, List<ScoreObject>>();
        scores.put(0, new ArrayList<ScoreObject>());
        scores.put(1, new ArrayList<ScoreObject>());
        scores.put(2, new ArrayList<ScoreObject>());
        scores.put(3, new ArrayList<ScoreObject>());

        Double[] inputArray;
        Double[] sorted;

        for(double i = 5; i < 10000; i *= 1.5)
        {
            System.out.println("We are at : " + i + " Elements! ");
            inputArray = getArrayWithRandomDobles((int)i);

            scores.get(0).add(
                    new ScoreObject(
                            "StandardMergesort",
                            (long)i,
                            new ExecutionTimer<StandardMergesort>(new StandardMergesort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(1).add(
                    new ScoreObject(
                            "ConcurrentMergesort",
                            (long)i,
                            new ExecutionTimer<ConcurrentMergesort>(new ConcurrentMergesort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(2).add(
                    new ScoreObject(
                            "StandardImprovedMergesort",
                            (long)i,
                            new ExecutionTimer<StandardImprovedMergesort>(new StandardImprovedMergesort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
            scores.get(3).add(
                    new ScoreObject(
                            "ConcurrentImprovedMergesort",
                            (long)i,
                            new ExecutionTimer<ConcurrentImprovedMergesort>(new ConcurrentImprovedMergesort(inputArray,0, inputArray.length - 1)).time / 1000000)
            );
        }

        GraphHandler graphHandler = new GraphHandler(scores);
        graphHandler.createAndShowGui();
    }

    private static Double[] getArrayWithRandomDobles(int size)
    {
        Double array[] = new Double[size];

        for (int i = 0; i < size; i++){
            array[i] = Math.random() * 1;
        }
        return array;
    }
}
