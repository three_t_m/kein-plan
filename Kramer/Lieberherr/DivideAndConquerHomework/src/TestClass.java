import ConncurrentFibonacci.FibonacciDNCConcurrent;
import DynamicFibonacci.DynamicFibonacci;
import StandardFibonacci.FibonacciDNC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class TestClass {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Map<Integer, List<Long>> scores = new HashMap<Integer, List<Long>>();

        System.out.println("NORMAL");
        FibonacciDNC normalFibonacciDnC;
        List<Long> times = new ArrayList<>();
        for (int i = 1; i < 30; i++) {
            normalFibonacciDnC = new FibonacciDNC(i);
            long time = new ExecutionTimer<Integer>(normalFibonacciDnC).time / 1000000;
            times.add(time);
            System.out.println("i = " + ((Integer) i).toString() + ": " + time);
        }
        scores.put(0, times);

        System.out.println("PARALLEL");
        FibonacciDNCConcurrent parallelFibonacciDnc;
        times = new ArrayList<>();
        for (int i = 1; i < 30; i++) {
            parallelFibonacciDnc = new FibonacciDNCConcurrent(i);
            long time = new ExecutionTimer<Integer>(parallelFibonacciDnc).time / 1000000;
            times.add(time);
            System.out.println("i = " + ((Integer) i).toString() + ": " + new ExecutionTimer<Integer>(parallelFibonacciDnc).time / 1000000);
        }
        scores.put(1, times);

        System.out.println("DYNAMIC");
        DynamicFibonacci dynamicFibonacci;
        times = new ArrayList<>();
        for (int i = 1; i < 30; i++) {
            dynamicFibonacci = new DynamicFibonacci(i);
            long time = new ExecutionTimer<Integer>(dynamicFibonacci).time / 1000000;
            times.add(time);
            System.out.println("i = " + ((Integer) i).toString() + ": " + new ExecutionTimer<Integer>(dynamicFibonacci).time / 1000000);
        }
        scores.put(2, times);

        GraphHandler graphHandler = new GraphHandler(scores);
        graphHandler.createAndShowGui();
    }
}