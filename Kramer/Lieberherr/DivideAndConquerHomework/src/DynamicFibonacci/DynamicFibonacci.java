package DynamicFibonacci;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

public class DynamicFibonacci implements DivideAndConquerableDynamic<Integer>, Supplier<Integer> {

    int n;

    public DynamicFibonacci(int n) {
        this.n = n;
        saved = new HashMap<Integer, Integer>(n + 2);
        saved.put(0, 0);
        saved.put(1, 1);
        saved.put(2, 1);
    }

    protected static HashMap<Integer, Integer> saved;

    @Override
    public boolean isBasic() {

        Integer i = saved.get(this.n);
        if (i == null) {
            return false;
        }
        return true;

    }

    @Override
    public Integer get() {
        return this.divideAndConquer();
    }

    @Override
    public Integer baseFun() {
        return saved.get(this.n);
    }

    @Override
    public List<DynamicFibonacci> decompose() {
        List<DynamicFibonacci> integers = new ArrayList<>();

        integers.add(new DynamicFibonacci(n - 1));
        integers.add(new DynamicFibonacci(n - 2));

        return integers;
    }

    @Override
    public Integer recombine(List<Integer> intermediateResults) {

        saved.put(n, intermediateResults.stream().mapToInt(Integer::intValue).sum());
        return saved.get(n);
    }
}