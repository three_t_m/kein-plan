package StandardFibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;


public class FibonacciDNC implements DivideAndConquerable<Integer>, Supplier<Integer> {

    int n;

    public FibonacciDNC(int n) {
        this.n = n;
    }

    @Override
    public boolean isBasic() {

        if (this.n < 3) {
            return true;
        }
        return false;
    }

    @Override
    public Integer get() {
        return this.divideAndConquer();
    }

    @Override
    public Integer baseFun() {
        return 1;
    }

    @Override
    public List<FibonacciDNC> decompose() {
        List<FibonacciDNC> integers = new ArrayList<>();

        integers.add(new FibonacciDNC(n - 1));
        integers.add(new FibonacciDNC(n - 2));

        return integers;
    }

    @Override
    public Integer recombine(List<Integer> intermediateResults) {
        return intermediateResults.stream().mapToInt(Integer::intValue).sum();
    }
}